import copy

board = []

def init():
    global board
    board = [
        [0, 0, 0, 0, 0, 0, 0, 2],
        [0, 0, 3, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 3, 4, 1, 0],
        [0, 0, 0, 0, 1, 2, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 0, 4, 0, 0, 0],
        [0, 2, 0, 0, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0]
    ]

def is_valid(array):
    for x in array:
        if not (x >= 0 and x <= 7):
            return False
    return True

def single_checkers_all_moves(row, col):
    moves = []
    # Black checker move
    if board[row][col] == 1:
        # If enemy (white checker or white dame)
        did_beat = False
        temp_board = copy.deepcopy(board)
        checker_row = row
        checker_col = col
        directions_for_black_checkers = [(1, -1), (1, 1)]
        i = 0
        while i < len(directions_for_black_checkers):
            direction = directions_for_black_checkers[i]
            direction_row = direction[0]
            direction_col = direction[1]
            i += 1
            while is_valid([checker_row + direction_row, checker_col + direction_col])\
                    and is_valid([checker_row + direction_row * 2, checker_col + direction_col * 2]) \
                    and temp_board[checker_row + direction_row][checker_col + direction_col] in [2, 4]\
                    and temp_board[checker_row + direction_row *2 ][checker_col + direction_col * 2] == 0:
                temp_board[checker_row + direction_row][checker_col + direction_col] = 0
                temp_board[checker_row + direction_row * 2][checker_col + direction_col * 2] = 1
                temp_board[checker_row][checker_col] = 0
                checker_row += direction_row * 2
                checker_col += direction_col * 2
                did_beat = True
                i = 0
                break
        if did_beat:
            moves.append(temp_board)
        # If empty
        if not did_beat:
            if board[row + 1][col - 1] == 0:
                temp_board = copy.deepcopy(board)
                temp_board[row + 1][col - 1] = 1
                temp_board[row][col] = 0
                moves.append(temp_board)
            if board[row + 1][col + 1] == 0:
                temp_board = copy.deepcopy(board)
                temp_board[row + 1][col + 1] = 1
                temp_board[row][col] = 0
                moves.append(temp_board)
    # Black dame move
    if board[row][col] == 3:
        # If enemy (white checker or white dame)
        did_beat = False
        directions = [(1, -1), (1, 1), (-1, -1), (-1, 1)]
        temp_board = copy.deepcopy(board)
        dame_row = row
        dame_col = col
        i = 0
        while i < len(directions):
            temp_row = dame_row
            temp_col = dame_col
            direction = directions[i]
            direction_row = direction[0]
            direction_col = direction[1]
            i += 1
            while is_valid([temp_row + direction_row, temp_col + direction_col]):
                cell_to_check = temp_board[temp_row + direction_row][temp_col + direction_col]
                if cell_to_check == 0:
                    temp_row += direction_row
                    temp_col += direction_col
                elif cell_to_check in [1, 3]:
                    break
                elif is_valid([temp_row + direction_row * 2, temp_col + direction_col * 2]) \
                        and temp_board[temp_row + direction_row * 2][temp_col + direction_col * 2] == 0:
                    temp_board[temp_row + direction_row][temp_col + direction_col] = 0
                    temp_board[temp_row + direction_row * 2][temp_col + direction_col * 2] = 3
                    temp_board[dame_row][dame_col] = 0
                    dame_row = temp_row + direction_row * 2
                    dame_col = temp_col + direction_col * 2
                    did_beat = True
                    i = 0
                    break
                else:
                    break
        if did_beat:
            moves.append(temp_board)
        # If empty
        if not did_beat:
            directions = [(1, -1), (1, 1), (-1, -1), (-1, 1)]
            for direction in directions:
                temp_row = row
                temp_col = col
                direction_row = direction[0]
                direction_col = direction[1]
                temp_board = copy.deepcopy(board)
                while is_valid([temp_row + direction_row, temp_col + direction_col]) and temp_board[temp_row + direction_row][temp_col + direction_col] == 0:
                    temp_board[temp_row + direction_row][temp_col + direction_col] = 3
                    temp_board[temp_row][temp_col] = 0
                    moves.append(copy.deepcopy(temp_board))
                    temp_row += direction_row
                    temp_col += direction_col
    # White checker move
    if board[row][col] == 2:
        # If enemy (black checker or black dame)
        did_beat = False
        temp_board = copy.deepcopy(board)
        checker_row = row
        checker_col = col
        directions_for_white_checkers = [(-1, -1), (-1, 1)]
        i = 0
        while i < len(directions_for_white_checkers):
            direction = directions_for_white_checkers[i]
            direction_row = direction[0]
            direction_col = direction[1]
            i += 1
            while is_valid([checker_row + direction_row, checker_col + direction_col]) \
                    and is_valid([checker_row + direction_row * 2, checker_col + direction_col * 2])\
                    and temp_board[checker_row + direction_row][checker_col + direction_col] in [1, 3] \
                    and temp_board[checker_row + direction_row * 2][checker_col + direction_col * 2] == 0:
                temp_board[checker_row + direction_row][checker_col + direction_col] = 0
                temp_board[checker_row + direction_row * 2][checker_col + direction_col * 2] = 2
                temp_board[checker_row][checker_col] = 0
                checker_row += direction_row * 2
                checker_col += direction_col * 2
                did_beat = True
                i = 0
                break
        if did_beat:
            moves.append(temp_board)
        # If empty
        if not did_beat:
            if board[row - 1][col - 1] == 0:
                temp_board = copy.deepcopy(board)
                temp_board[row - 1][col - 1] = 2
                temp_board[row][col] = 0
                moves.append(temp_board)
            if board[row - 1][col + 1] == 0:
                temp_board = copy.deepcopy(board)
                temp_board[row - 1][col + 1] = 2
                temp_board[row][col] = 0
                moves.append(temp_board)
    # White dame move
    if board[row][col] == 4:
        # If enemy (black checker or black dame)
        did_beat = False
        directions = [(1, -1), (1, 1), (-1, -1), (-1, 1)]
        temp_board = copy.deepcopy(board)
        dame_row = row
        dame_col = col
        i = 0
        while i < len(directions):
            temp_row = dame_row
            temp_col = dame_col
            direction = directions[i]
            direction_row = direction[0]
            direction_col = direction[1]
            i += 1
            while is_valid([temp_row + direction_row, temp_col + direction_col]):
                cell_to_check = temp_board[temp_row + direction_row][temp_col + direction_col]
                if cell_to_check == 0:
                    temp_row += direction_row
                    temp_col += direction_col
                elif cell_to_check in [2, 4]:
                    break
                elif is_valid([temp_row + direction_row * 2, temp_col + direction_col * 2])\
                        and temp_board[temp_row + direction_row * 2][temp_col + direction_col * 2] == 0:
                    temp_board[temp_row + direction_row][temp_col + direction_col] = 0
                    temp_board[temp_row + direction_row * 2][temp_col + direction_col * 2] = 4
                    temp_board[dame_row][dame_col] = 0
                    dame_row = temp_row + direction_row * 2
                    dame_col = temp_col + direction_col * 2
                    did_beat = True
                    i = 0
                    break
                else:
                    break
        if did_beat:
            moves.append(temp_board)
        # If empty
        if not did_beat:
            directions = [(1, -1), (1, 1), (-1, -1), (-1, 1)]
            for direction in directions:
                temp_row = row
                temp_col = col
                direction_row = direction[0]
                direction_col = direction[1]
                temp_board = copy.deepcopy(board)
                while is_valid([temp_row + direction_row, temp_col + direction_col]) and \
                                temp_board[temp_row + direction_row][temp_col + direction_col] == 0:
                    temp_board[temp_row + direction_row][temp_col + direction_col] = 4
                    temp_board[temp_row][temp_col] = 0
                    moves.append(copy.deepcopy(temp_board))
                    temp_row += direction_row
                    temp_col += direction_col
    return moves

def draw_single_checkers_moves(row, col):
    moves = single_checkers_all_moves(row, col)
    for g in moves:
        for r in range(8):
            for c in range(8):
                print("{}".format(g[r][c]), end="")
            print("")
        print("")

def convert_to_dame():
    for index, element in enumerate(board[0]):
        if element == 2:
            board[0][index] = 4
    for index, element in enumerate(board[len(board) - 1]):
        if element == 1:
            board[len(board) - 1][index] = 3

def draw_board(board):
    header = "  A|B|C|D|E|F|G|H "
    header_2 = "  ————————————————"
    footer = "  ————————————————"
    symbol = ""
    print(header)
    print(header_2)
    for row in range(len(board)):
        for col in range(len(board)):
            if board[row][col] == 0 and ((row % 2 == 0 and col % 2 == 0) or (row % 2 != 0 and col % 2 != 0)):
                symbol = "░░"
            elif board[row][col] == 0 and ((row % 2 == 0 and col %2 != 0) or (row % 2 != 0 and col %2 == 0)):
                symbol = "▓▓"
            elif board[row][col] == 1:
                symbol = " ♂"
            elif board[row][col] == 3:
                symbol = " ∑"
            elif board[row][col] == 2:
                symbol = " ♀"
            elif board[row][col] == 4:
                symbol = " ∩"
            if col == 0:
                symbol = "{}|{}".format(row + 1, symbol)
            elif col == len(board) - 1:
                symbol = "{}|".format(symbol)
            print(symbol, end="")
        print("")
    print(footer)

def beat_possible(row, col):
    cell_to_check = board[row][col]
    is_dame = False
    directions = []
    enemies = []
    if cell_to_check in [4, 3]:
        is_dame = True
        directions = [(1, 1), (1, -1),(-1, 1), (-1, -1)]
        enemies = [1, 3] if cell_to_check == 4 else [2, 4]
    else:
        directions = [(-1, 1), (-1, -1)] if cell_to_check == 2 else [(1, 1), (1, -1)]
        enemies = [1, 3] if cell_to_check == 2 else [2, 4]
    if is_dame:
        i = 0
        while i < len(directions):
            temp_input_row = row
            temp_input_col = col
            direction = directions[i]
            direction_row = direction[0]
            direction_col = direction[1]
            i += 1
            while (is_valid([temp_input_row + direction_row * 2, temp_input_col + direction_col * 2])):
                if board[temp_input_row + direction_row][temp_input_col + direction_col] == 0:
                    temp_input_row += direction_row
                    temp_input_col += direction_col
                elif board[temp_input_row + direction_row][temp_input_col + direction_col] in enemies \
                        and board[temp_input_row + direction_row * 2][temp_input_col + direction_col * 2] == 0:
                    return True
                else:
                    break
    else:
        i = 0
        while i < len(directions):
            direction = directions[i]
            direction_row = direction[0]
            direction_col = direction[1]
            i += 1
            if (is_valid([row + direction_row * 2, col + direction_col * 2])) and (board[row + direction_row * 2][col + direction_col * 2] == 0) \
                    and (board[row + direction_row][col + direction_col] in enemies):
                    return True
    return False

def beat_possible_for_whole_board(board, player_1_turn):
    for row in range(len(board)):
        for col in range(len(board)):
            cell_to_check = board[row][col]
            if player_1_turn and cell_to_check in [2, 4] and beat_possible(row, col):
                return True
            elif not player_1_turn and cell_to_check in [1, 3] and beat_possible(row, col):
                return True
    return False

def beat_possible_for_direction(row, col, temp_board, direction):
    cell_to_check = temp_board[row][col]
    is_dame = False
    enemies = []
    if cell_to_check in [4, 3]:
        is_dame = True
        enemies = [1, 3] if cell_to_check == 4 else [2, 4]
    else:
        enemies = [1, 3] if cell_to_check == 2 else [2, 4]
    if is_dame:
        temp_input_row = row
        temp_input_col = col
        direction_row = direction[0]
        direction_col = direction[1]
        while (is_valid([temp_input_row + direction_row * 2, temp_input_col + direction_col * 2])):
            if temp_board[temp_input_row + direction_row][temp_input_col + direction_col] == 0:
                temp_input_row += direction_row
                temp_input_col += direction_col
            elif temp_board[temp_input_row + direction_row][temp_input_col + direction_col] in enemies \
                    and temp_board[temp_input_row + direction_row * 2][temp_input_col + direction_col * 2] == 0:
                temp_board[temp_input_row + direction_row * 2][temp_input_col + direction_col * 2] = cell_to_check
                temp_board[row][col] = 0
                temp_board[temp_input_row + direction_row][temp_input_col + direction_col] = 0
                new_row = temp_input_row + direction_row * 2
                new_col = temp_input_col + direction_col * 2
                return (True, new_row, new_col, copy.deepcopy(temp_board))
            else:
                break
    else:
        direction_row = direction[0]
        direction_col = direction[1]
        if (is_valid([row + direction_row * 2, col + direction_col * 2])) and (temp_board[row + direction_row * 2][col + direction_col * 2] == 0) \
                and (temp_board[row + direction_row][col + direction_col] in enemies):
            temp_board[row + direction_row * 2][col + direction_col * 2] = cell_to_check
            temp_board[row][col] = 0
            temp_board[row + direction_row][col + direction_col] = 0
            new_row = row + direction_row * 2
            new_col = col + direction_col * 2
            return (True, new_row, new_col, copy.deepcopy(temp_board))
    return (False, 0, 0, 0)

def beating(row, col):
    cell_to_check = board[row][col]
    is_dame = False
    enemies = []
    if cell_to_check in [4, 3]:
        is_dame = True
        enemies = [1, 3] if cell_to_check == 4 else [2, 4]
    else:
        enemies = [1, 3] if cell_to_check == 2 else [2, 4]
    if is_dame:
        temp_dame_from_row = row
        temp_dame_from_col = col
        while beat_possible(temp_dame_from_row, temp_dame_from_col):
            current_pos_row = temp_dame_from_row
            current_pos_col = temp_dame_from_col
            direction = beating_max_2(temp_dame_from_row, temp_dame_from_col)
            direction_row = direction[0]
            direction_col = direction[1]
            while is_valid([current_pos_row + direction_row * 2, current_pos_col + direction_col * 2]):
                if board[current_pos_row + direction_row][current_pos_col + direction_col] == 0:
                    current_pos_row += direction_row
                    current_pos_col += direction_col
                elif board[current_pos_row + direction_row][current_pos_col + direction_col] in enemies \
                        and board[current_pos_row + direction_row * 2][current_pos_col + direction_col * 2] == 0:
                    board[current_pos_row + direction_row * 2][current_pos_col + direction_col * 2] = cell_to_check
                    board[temp_dame_from_row][temp_dame_from_col] = 0
                    board[current_pos_row + direction_row][current_pos_col + direction_col] = 0
                    temp_dame_from_row = current_pos_row + direction_row * 2
                    temp_dame_from_col = current_pos_col + direction_col * 2
                    current_pos_row = temp_dame_from_row
                    current_pos_col = temp_dame_from_col
                else:
                    break
    else:
        current_pos_row = row
        current_pos_col = col
        while beat_possible(current_pos_row, current_pos_col):
            direction = beating_max_2(current_pos_row, current_pos_col)
            direction_row = direction[0]
            direction_col = direction[1]
            if (is_valid([current_pos_row + direction_row * 2, current_pos_col + direction_col * 2])) \
                    and (board[current_pos_row + direction_row * 2][current_pos_col + direction_col * 2] == 0) \
                    and (board[current_pos_row + direction_row][current_pos_col + direction_col] in enemies):
                board[current_pos_row + direction_row * 2][current_pos_col + direction_col * 2] = cell_to_check
                board[current_pos_row + direction_row][current_pos_col + direction_col] = 0
                board[current_pos_row][current_pos_col] = 0
                current_pos_row += direction_row * 2
                current_pos_col += direction_col * 2

def beating_max_2(row, col):
    cell_to_check = board[row][col]
    directions = []
    if cell_to_check in [4, 3]:
        directions = [(1, 1), (1, -1), (-1, 1), (-1, -1)]
    else:
        directions = [(-1, 1), (-1, -1)] if cell_to_check == 2 else [(1, 1), (1, -1)]
    max_number = 0
    max_direction = directions[0]
    for direction in directions:
        temp_max_number = beating_max_for_direction(
            direction,
            row,
            col,
            directions,
            copy.deepcopy(board)
        )
        if temp_max_number > max_number:
            max_number = temp_max_number
            max_direction = direction
    return max_direction

def beating_max_for_direction(direction, row, col, permitted_directions, temp_board, current_beat_num = 0):
    result = beat_possible_for_direction(row, col, temp_board, direction)
    if result[0]:
        max_number = 0
        for direction in permitted_directions:
            temp_max_number = beating_max_for_direction(
                direction,
                result[1],
                result[2],
                permitted_directions,
                result[3],
                current_beat_num + 1
            )
            if temp_max_number > max_number:
                max_number = temp_max_number
        return max_number
    return current_beat_num

def game_over(array):
    black_checkers = 0
    white_checkers = 0
    for row in array:
        for col in row:
            if col in [1, 3]:
                black_checkers += 1
            if col in [2, 4]:
                white_checkers += 1
    if black_checkers == 0 or white_checkers == 0:
        print("The winner is player {}.".format(1 if black_checkers == 0 else 2))
        return True
    return False

init()
draw_board(board)
player_1_turn = True
while not game_over(board):
    player_input_from = input("Player {} from: ".format(1 if player_1_turn else 2))
    player_input_to = input("Player {} to: ".format(1 if player_1_turn else 2))
    input_array_from = player_input_from.split(",")
    input_array_from[0] = int(input_array_from[0])
    input_array_from[1] = int(input_array_from[1])
    input_array_to = player_input_to.split(",")
    input_array_to[0] = int(input_array_to[0])
    input_array_to[1] = int(input_array_to[1])
    if is_valid(input_array_from + input_array_to):
        if board[input_array_from[0]][input_array_from[1]] != 0:
            # All white checkers
            # White checker (2)
            if player_1_turn and board[input_array_from[0]][input_array_from[1]] == 2:
                if (input_array_to[0] < input_array_from[0]) and (input_array_to[1] != input_array_from[1]):
                    # White checker beat (2)
                    if (input_array_to[0] - input_array_from[0] == -2) and (input_array_to[1] - input_array_from[1] in [2, -2]):
                        direction_row = -1
                        if input_array_to[1] - input_array_from[1] == -2:
                            direction_col = -1
                        else:
                            direction_col = 1
                        directions = [(direction_row, direction_col), (direction_row, direction_col * -1)]
                        temp_input_row = input_array_from[0]
                        temp_input_col = input_array_from[1]
                        if board[temp_input_row + direction_row] [temp_input_col + direction_col] in [1, 3] and board[input_array_to[0]][input_array_to[1]] == 0:
                            board[temp_input_row + direction_row * 2][temp_input_col + direction_col * 2] = 2
                            board[temp_input_row + direction_row][temp_input_col + direction_col] = 0
                            board[temp_input_row][temp_input_col] = 0
                            temp_input_row += direction_row * 2
                            temp_input_col += direction_col * 2
                            beating(temp_input_row, temp_input_col)
                            convert_to_dame()
                            draw_board(board)
                            player_1_turn = not player_1_turn
                        else:
                            print("Enter correct direction where to move. There is no enemy or cell after enemy is not empty.")
                    #elif not beat_possible(input_array_from[0], input_array_from[1]):
                    elif not beat_possible_for_whole_board(board, player_1_turn):
                        #  White checker move only (2)
                        if (input_array_to[0] - input_array_from[0] == -1) and (input_array_to[1] - input_array_from[1] in [1, -1]) \
                                and board[input_array_to[0]][input_array_to[1]] == 0:
                            board[input_array_to[0]][input_array_to[1]] = 2
                            board[input_array_from[0]][input_array_from[1]] = 0
                            player_1_turn = not player_1_turn
                            convert_to_dame()
                            draw_board(board)
                        else:
                            print("Enter correct direction where to move. You can't move this way or cell is not empty.")
                    else:
                        print("Enter correct direction. You have to beat.")
                else:
                    print("Wrong direction. Enter correct direction where to move.")
            # White dame (4)
            elif player_1_turn and board[input_array_from[0]][input_array_from[1]] == 4:
                if (abs(input_array_to[0] - input_array_from[0]) == abs(input_array_to[1] - input_array_from[1])) and board[input_array_to[0]][input_array_to[1]] == 0:
                    if input_array_to[0] - input_array_from[0] > 0:
                        direction_row = 1
                    else:
                        direction_row = -1
                    if input_array_to[1] - input_array_from[1] > 0:
                        direction_col = 1
                    else:
                        direction_col = -1
                    # White dame beat (4)
                    directions = [(direction_row, direction_col), (direction_row, direction_col * -1), (direction_row * -1, direction_col), (direction_row * -1, direction_col * -1)]
                    if board[input_array_to[0] - direction_row][input_array_to[1] - direction_col] in [1,3]:
                        cell_with_enemy_row = input_array_to[0] - direction_row
                        cell_with_enemy_col = input_array_to[1] - direction_col
                        temp_input_row = input_array_from[0]
                        temp_input_col = input_array_from[1]
                        temp_dame_from_row = input_array_from[0]
                        temp_dame_from_col = input_array_from[1]
                        can_beat = True
                        while (temp_input_row + direction_row != cell_with_enemy_row) and (temp_input_col + direction_col != cell_with_enemy_col):
                            if board[temp_input_row + direction_row][temp_input_col + direction_col] == 0:
                                temp_input_row += direction_row
                                temp_input_col += direction_col
                            else:
                                can_beat = False
                                print("Enter correct direction. Some cells are not empty.")
                                break
                        if can_beat:
                            board[cell_with_enemy_row + direction_row][cell_with_enemy_col + direction_col] = 4
                            board[temp_dame_from_row][temp_dame_from_col] = 0
                            board[cell_with_enemy_row][cell_with_enemy_col] = 0
                            temp_dame_from_row = cell_with_enemy_row + direction_row
                            temp_dame_from_col = cell_with_enemy_col + direction_col
                            temp_input_row = temp_dame_from_row
                            temp_input_col = temp_dame_from_col
                            beating(temp_dame_from_row, temp_dame_from_col)
                            draw_board(board)
                            player_1_turn = not player_1_turn
                    elif not beat_possible_for_whole_board(board, player_1_turn):
                        # White dame move only (4)
                        temp_input_row = input_array_from[0]
                        temp_input_col = input_array_from[1]
                        can_move = True
                        while (temp_input_row + direction_row != input_array_to[0]) and (temp_input_col + direction_col != input_array_to[1]):
                            if board[temp_input_row + direction_row][temp_input_col + direction_col] == 0:
                                temp_input_row += direction_row
                                temp_input_col += direction_col
                            else:
                                can_move = False
                                print("Enter correct direction. Some cells are not empty.")
                                break
                        if can_move:
                            board[input_array_to[0]][input_array_to[1]] = 4
                            board[input_array_from[0]][input_array_from[1]] = 0
                            draw_board(board)
                            player_1_turn = not player_1_turn
                    else:
                        print("Enter correct direction. You have to beat.")
                else:
                    print("Enter correct direction where to move. You can't move this way or cell is not empty.")
            # All black checkers
            # Black checker move (1)
            elif not player_1_turn and board[input_array_from[0]][input_array_from[1]] == 1:
                if (input_array_to[0] > input_array_from[0]) and (input_array_to[1] != input_array_from[1]):
                    # Black checker beat (1)
                    if (input_array_to[0] - input_array_from[0] == 2) and (input_array_to[1] - input_array_from[1] in [2, -2]):
                        direction_row = 1
                        if input_array_to[1] - input_array_from[1] == -2:
                            direction_col = -1
                        else:
                            direction_col = 1
                        directions = [(direction_row, direction_col), (direction_row, direction_col * -1)]
                        temp_input_row = input_array_from[0]
                        temp_input_col = input_array_from[1]
                        if board[temp_input_row + direction_row][temp_input_col + direction_col] in [2, 4] and board[input_array_to[0]][input_array_to[1]] == 0:
                            board[temp_input_row + direction_row * 2][temp_input_col + direction_col * 2] = 1
                            board[temp_input_row + direction_row][temp_input_col + direction_col] = 0
                            board[temp_input_row][temp_input_col] = 0
                            temp_input_row += direction_row * 2
                            temp_input_col += direction_col * 2
                            beating(temp_input_row, temp_input_col)
                            convert_to_dame()
                            draw_board(board)
                            player_1_turn = not player_1_turn
                        else:
                            print("Enter correct direction where to move. There is no enemy or cell after enemy is not empty.")
                    elif not beat_possible_for_whole_board(board, player_1_turn):
                        # Black checker move only (1)
                        if (input_array_to[0] - input_array_from[0] == 1) and (input_array_to[1] - input_array_from[1] in [1, -1]) \
                                and board[input_array_to[0]][input_array_to[1]] == 0:
                            board[input_array_to[0]][input_array_to[1]] = 1
                            board[input_array_from[0]][input_array_from[1]] = 0
                            player_1_turn = not player_1_turn
                            convert_to_dame()
                            draw_board(board)
                        else:
                            print("Enter correct direction where to move. You can't move this way or cell is not empty.")
                    else:
                        print("Enter correct direction. You have to beat.")
                else:
                    print("Wrong direction. Enter correct direction where to move.")
            # Black dame (3)
            elif not player_1_turn and board[input_array_from[0]][input_array_from[1]] == 3:
                if (abs(input_array_to[0] - input_array_from[0]) == abs(input_array_to[1] - input_array_from[1])) and board[input_array_to[0]][input_array_to[1]] == 0:
                    if input_array_to[0] - input_array_from[0] > 0:
                        direction_row = 1
                    else:
                        direction_row = -1
                    if input_array_to[1] - input_array_from[1] > 0:
                        direction_col = 1
                    else:
                        direction_col = -1
                    # Black dame beat (3)
                    directions = [(direction_row, direction_col), (direction_row, direction_col * -1),
                                  (direction_row * -1, direction_col), (direction_row * -1, direction_col * -1)]
                    if board[input_array_to[0] - direction_row][input_array_to[1] - direction_col] in [2, 4]:
                        cell_with_enemy_row = input_array_to[0] - direction_row
                        cell_with_enemy_col = input_array_to[1] - direction_col
                        temp_input_row = input_array_from[0]
                        temp_input_col = input_array_from[1]
                        temp_dame_from_row = input_array_from[0]
                        temp_dame_from_col = input_array_from[1]
                        can_beat = True
                        while (temp_input_row + direction_row != cell_with_enemy_row) and (temp_input_col + direction_col != cell_with_enemy_col):
                            if board[temp_input_row + direction_row][temp_input_col + direction_col] == 0:
                                temp_input_row += direction_row
                                temp_input_col += direction_col
                            else:
                                can_beat = False
                                print("Enter correct direction. Some cells are not empty.")
                                break
                        if can_beat:
                            board[cell_with_enemy_row + direction_row][cell_with_enemy_col + direction_col] = 3
                            board[temp_dame_from_row][temp_dame_from_col] = 0
                            board[cell_with_enemy_row][cell_with_enemy_col] = 0
                            temp_dame_from_row = cell_with_enemy_row + direction_row
                            temp_dame_from_col = cell_with_enemy_col + direction_col
                            temp_input_row = temp_dame_from_row
                            temp_input_col = temp_dame_from_col
                            beating(temp_dame_from_row, temp_dame_from_col)
                            draw_board(board)
                            player_1_turn = not player_1_turn
                    elif not beat_possible_for_whole_board(board, player_1_turn):
                        # Black dame move only (3)
                        temp_input_row = input_array_from[0]
                        temp_input_col = input_array_from[1]
                        can_move = True
                        while (temp_input_row + direction_row != input_array_to[0]) and (temp_input_col + direction_col != input_array_to[1]):
                            if board[temp_input_row + direction_row][temp_input_col + direction_col] == 0:
                                temp_input_row += direction_row
                                temp_input_col += direction_col
                            else:
                                can_move = False
                                print("Enter correct direction. Some cells are not empty.")
                                break
                        if can_move:
                            board[input_array_to[0]][input_array_to[1]] = 3
                            board[input_array_from[0]][input_array_from[1]] = 0
                            draw_board(board)
                            player_1_turn = not player_1_turn
                    else:
                        print("Enter correct direction. You have to beat.")
                else:
                    print("Enter correct direction where to move. You can't move this way or cell is not empty.")
            else:
                print("This is not your checker.")
        else:
            print("There is no checker.")
    else:
        print("Enter correct direction. Your direction is out of range.")